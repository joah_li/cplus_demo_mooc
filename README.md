# cplus_demo_mooc

mooc c++ 项目

## 补码

![avatar](./补码.png)

## unicode

![avatar](./unicode.png)

## C++ 中内存的存储

![avatar](./内存地址.png)

## C++ 中几种变量的对比

![avatar](./变量对比.png)

## C++ 中变量的存储区域的对比

![avatar](./变量存储区域.png)

## shared_ptr 智能指针

![avatar](./shared_ptr.png)

## weak_ptr 智能指针(弱指针)

![avatar](./weak_ptr.png)

## unique_ptr 智能指针

![avatar](./unique_ptr.png)
