#include <iostream>
#include <cassert>

using namespace std;

// 二进制转有符号整数

int b2t(int num)
{
  return (int)(num);
}

// 二进制转无符号整数
unsigned int b2u(int num)
{
  return (unsigned int)(num);
}

int main(int argc, const char **argv)
{

  int m = 10;
  char str[10] = "a";

  short int sInt = 88;

  // int m = 94;

  long double k = 87.00;

  bool b = true;

  wchar_t w[10] = L"a";

  assert(b == true);

  cout << "get ret: " << *w << endl;

  cout << "get ret: fuck" << endl;

  // note: 使用 加法 来做 减法

  // a - b => a + (-b)

  // b => -b
  // 3 => -3
  // 比如：时间， 转上一圈， 3 点，转上一圈 12 得到 9， 这里就得到一个映射表，
  // 那么在计算机内部也有一个映射表， 这个就是补码
  // 0000 0000 0000 0000 0000 0000 0000 0011 => 3 得到 -3 就是 3 保留符号位，之后的所有的位取补码， 最后再 + 1；
  // 保留符号位 1 => 后面都去补码 0 变为 1 =>
  // 1111 1111 1111 1111 1111 1111 1111 1100 + 1 => 得到如下
  // 1111 1111 1111 1111 1111 1111 1111 1101 => -3

  // 二进制的加法： 按位 与：
  //  0000 0000 0000 0000 0000 0000 0000 0011 => 3
  //  1111 1111 1111 1111 1111 1111 1111 1101 => -3
  //1 0000 0000 0000 0000 0000 0000 0000 0000 => 0 => 去掉溢出最高位

  // .... 0001  => 1
  // 求 负数： num = -x * 2 ** (w - 1) + 2 ** (w-2) ..... 2 ** 0;
  // .... 1111  => -1 => -2**3 + 2**2 + 2**1 + 2**0 = -8 + 4 + 2 + 1 = -1

  int i1 = 0;
  int i2 = -1;
  int i3 = -2147483648; // => Intel 中存储的小端的存储法： 00 00 00 80, 如果是大端的标识法： 80 00 00 00
  int i4 = 2147483647;

  cout << "b2t(0x00000000)" << b2t(0x00000000) << endl; // 0
  cout << "b2t(0xffffffff)" << b2t(0xffffffff) << endl; // -1
  cout << "b2t(0x80000000)" << b2t(0x80000000) << endl; // -2147483648
  cout << "b2t(0x80000001)" << b2t(0x80000001) << endl; // -2147483647
  cout << "b2t(0x7fffffff)" << b2t(0x7fffffff) << endl; // 2147483647

  cout << "b2u(0x00000000)" << b2u(0x00000000) << endl; // 0
  cout << "b2u(0xffffffff)" << b2u(0xffffffff) << endl; // 4294967295
  cout << "b2u(0x80000000)" << b2u(0x80000000) << endl; // 2147483648
  cout << "b2u(0x7fffffff)" << b2u(0x7fffffff) << endl; // 2147483647

  return 0;
}

// keyword:
/*
  mutable static_cast, reinterpret_cast, register, explicit, typeid, union
  // 欧拉公式： e**PI * i + 1 = 0;
*/