#ifndef __ARRAY__
#define __ARRAY__

// NOTE: template 的声明和定义可以放在一个地方
#include <iostream>

using std::cout;
using std::endl;

template <class T> class Array {
private:
  int size = 0;
  // 声明一个指针类型
  T *data;

public:
  Array(int cap);
  ~Array();
  void push(T e);
  void print();
};

template <class T> Array<T>::Array(int cap) {
  // size = cap;
  // 动态分配内存
  data = new T[cap];
}

template <class T> Array<T>::~Array() { delete[] data; }

template <class T> void Array<T>::push(T e) {
  data[size] = e;
  size++;
}

template <class T> void Array<T>::print() {
  for (int i = 0; i < size; i++) {
    cout << "数组下标：" << i << "位置的值为：" << data[i] << endl;
  }
}

#endif // !1