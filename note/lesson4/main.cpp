#include <iostream>
// vector 动态数组
#include <vector>

// string header
// #include <string.h>

// NOTE: 统一使用 c++ 中的 string 类型， 不使用 c 中的 string.h 文件
#include <string>

#include "array.h"

using namespace std;

const int LENGTH = 10;

void array_test() {
  Array<int> arr(LENGTH);
  // 数组的 length sizeof(arr) / sizeof(arr[0])

  for (int i = 0; i < LENGTH; i++) {
    arr.push(i);
  }

  arr.print();
}

// NOTE: 动态数组
void vector_test() {
  vector<int> vec = {1, 2, 3, 4};
  cout << "vec 的 size()" << vec.size() << endl;
  cout << "vec 的 capacity()" << vec.capacity() << endl;
  // cout << "vec 的 end()" << vec.end() << endl;

  vec.push_back(10);
  vec.push_back(11);
  vec.push_back(12);
  cout << "vec 的 push_back()" << endl;

  cout << "vec 的 size()" << vec.size() << endl;
  cout << "vec 的 capacity()" << vec.capacity() << endl;
  // cout << "vec 的 end()" << vec.end() << endl;

  vec.insert(vec.begin(), 2);
  cout << "vec 的 insert()" << endl;

  cout << "vec 的 size()" << vec.size() << endl;
  cout << "vec 的 capacity()" << vec.capacity() << endl;
  // cout << "vec 的 end()" << vec.end() << endl;

  // 移除一个范围的数据
  vec.erase(vec.end() - 1); // 删除了最后一个值
  vec.erase(vec.begin());   // 删除了第一个一个值

  for (int i = 0; i < vec.size(); i++) {
    cout << "值为" << vec[i] << endl;
  }

  cout << "vec 的 erase()" << endl;
  cout << "vec 的 size()" << vec.size() << endl;
  cout << "vec 的 capacity()" << vec.capacity() << endl;
  // cout << "vec 的 end()" << vec.end() << endl;
  // /home/joah/.config/Code/User/globalStorage/llvm-vs-code-extensions.vscode-clangd/install/10.0.0/clangd_10.0.0/bin/clangd
}

void string_test() { string str = ""; }

int main(int argc, char const *argv[]) {

  // array_test();
  // vector_test();
  string_test();
  return 0;
}
