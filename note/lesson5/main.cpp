#include <cstring>
#include <iostream>
#include <memory>
#include <string>

using namespace std;

void variable_test() {
  // int b = 1;   // (stack)栈区变量
  // int *p = &b; // 栈区变量
  // char chars[] = {'1', '2', '3'};
  // string str = "123";

  static int c = 0; // (GVAR) 全局(静态)初始化区
  cout << c << endl;

  // int *ptr = NULL;
  // ptr = new int(10); // (heap) 堆区变量, 使用 new 分配堆的内存

  char *p_char_dest = new char[7];
  char p_char_src[] = {'1', '2', '3'};

  strcpy(p_char_dest, p_char_src); // text 代码区
  cout << p_char_dest << endl;
}

/*
  1. const 的修饰： 左侧最近的部分，如果左侧没有，则看右侧
 */

void const_variable_test() {
  // char chars[2] = {'a'};
  // const pointer
  // NOTE: => char* const pStr2 = "hello world"; 这个两个是等价的
  // char const *pStr1 = "hello world";

  // *pStr1 = "fuck"; // error
  // pStr1 = chars; // NOTE: 指针可以改变， 因为 const 并没有修饰到 pStr
  // 的指针，而且值，

  // pointer to const
  // char const *const pStr3 = "hello world";
  // pStr3 = chars; // error NOTE: 这里又修饰了指针， 又修饰了值都是 const
  // pStr3 = "fuck"; // error

  int a = 10;

  int *b = &a;

  int **c = &b;

  cout << "c 指针的值为：" << c << endl;

  // NOTE: 未初始化， 非法的指针,
  // NOTE: m 指针没有初始化，那么这里的 m 指针会指向哪里？？？ =>
  // 运气好的话，会定位到一个非法的地址，程序会出错,
  // 如果是定位到一个可以访问的地址，如果无意修改了，
  // 那么会导致难以定位到的错误，因为这里的操作与原先的操作完全不相关
  int *m = NULL; // 初始化为一个 NULL 指针， 不指向任何地方
  *m = 12;

  if (m != NULL) {
    cout << "m 指针的值为：" << *m << endl;
  }

  // NOTE: 野指针，指向了 "垃圾“ 内存的指针， if 判断对其没有作用

  /*
    NOTE:
      1. 指针变量没有初始化
      2. 已经释放不用的指针，没有置为 NULL, 如 delete  和 free 之后的指针
      3. 指针操作超越了变量的作用范围(比如指针指向了某个空间，
    这个空间的范围，或者是生命周期是有限的， 在生命周期之后还指向这个指针， 如在
    scope 之外的)

    NOTE:
      没有初始化的， 不用的，或者是超出范围的指针，请将其置为 NULL
   */
}

// NOTE: 智能指针
/**
 * unique_ptr, shared_ptr, weak_ptr 和在 C++11 中废弃的 auto_ptr， 在C++17
 * 中废弃 shared_ptr 多用， weak_ptr 其次， unique_ptr 少用
 * 应用场景：
 * 智能指针会自动的完成回收指针的操作
 */
/*
auto_ptr: 指针所有权失效， 在安全性会有问题, 在指针赋值的时候，会将原来指向的指针剥夺，再指向新的对象
然后再将原对象的指针变为 nullptr，
 */
void smart_ptr() {
  // auto_ptr 存在失效的范围，
  auto_ptr<int> p(new int(10));
  cout << *p << endl;
}

int main(int argc, const char **argv) {

  // variable_test();
  smart_ptr();
  return 0;
}