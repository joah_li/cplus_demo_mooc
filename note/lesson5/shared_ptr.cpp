// NOTE: 共享同一个对象的智能指针
// 循环引用： 引用计数会带来循环引用的问题
// 循环引用会导致堆里面的内存无法正常回收导致 内存泄露

#include <iostream>
#include <memory>

using namespace std;

int main(int argc, char const *argv[]) {

  // shared_ptr<int> p1(new int(10));
  // shared_ptr<int> p2(new int(1));
  // // NOTE: 这里会造成循环引用
  // p2 = p1;
  // p1 = p2;

  {
    auto wa = shared_ptr<int>(new int(20));
    {
      // 获取 wa 的指针, 使用 move 函数来移动指针
      auto wa2 = move(wa);

      cout << ((wa.get() != nullptr) ? (*wa2.get()) : -1) << endl;
    }
  }

  auto w = make_shared<int>(30);

  auto wa2 = move(w);

  int m = *w.get();   // -1
  int n = *wa2.get(); // 30

  w.use_count();   // 0
  wa2.use_count(); // 1

  return 0;
}

// DEBUG: clang++ -g -Wall -std=c++11 -stdlib=libc++ note/lesson5/shared_ptr.cpp
// -o bin/shared_ptr
