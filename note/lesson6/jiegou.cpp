#include <cstring>
#include <iostream>
using namespace std;

const int LENGATH = 6;

// union 联合体, double 和 char 共同使用同一个内存空间，使用最大的空间
union Score {
  double sc;  // 8
  char level; // 1 => 4，
};

// INFO: 结构体的内部的声明的顺序不一样，会导致内存的大小不一样
// 设置结构体内存数据对齐，需要修改编译选项
struct Student {
  char name[LENGATH]; // 6 => 8
  int age;            // 4 => 8
  Score s;            // => 8
}; // INFO: 这里要是最大元素的字节数的倍数，也就 8 的倍数 24 =>
// INFO: 32 位的内存地址是每 4 个字节为一个存储单元，
// 会以结构中字段最大的字段所占的字节数的整数倍

int main(int argc, char const *argv[]) {
  Student stu;
  // 字符串赋值不能使用 =， 要使用 copy
  strcpy(stu.name, "island");

  if (stu.age >= 10)
  {
    /* code */
  }

  stu.age = 15;
  stu.s.level = 'A';
  stu.s.sc = 95.9;
  cout << "name: " << stu.name << endl;
  cout << "sizeof: Score" << sizeof(Score) << endl;     // 8
  cout << "sizeof: Student" << sizeof(Student) << endl; // 24
  return 0;
}

// INFO: 数据结构岑差不齐会导致内存浪费