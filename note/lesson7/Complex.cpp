#include "Complex.h"
#include <iostream>

using namespace std;

Complex::Complex(double _real, double _image) {
  this->_real = _real;
  this->_image = _image;
}

Complex::~Complex() {}

double Complex::real() const { return _real; }
