class Complex {
private:
  double _real;  // note: 标示复数的实部
  double _image; // note: 标示复数的虚部

public:
  Complex(double _real, double _image);
  virtual ~Complex();

  // info: const 修饰的是 real 函数， 在函数内部，成员变量的值不能改变
  double real() const;
  double image() const { return _image; }
};