#include "Complex.h"
#include <iostream>

using namespace std;

int main(int argc, char const *argv[]) {
  Complex c(1.0, 2.0);

  cout << "_image : " << c.image() << endl;
  cout << "_real : " << c.real() << endl;

  /* code */
  return 0;
}
